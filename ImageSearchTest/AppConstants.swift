//
//  AppConstants.swift
//  ImageSearchTest
//
//  Created by DeveloperMBPRO on 9/19/19.
//  Copyright © 2019 Test project. All rights reserved.
//

import UIKit

class AppConstants: NSObject {
    private enum Constants {
        static let RalewayFont = "Raleway"
        static let FontSize: CGFloat = 20
    }
    static let MainColor = UIColor.colorFromHex(hex: "#73D4FF")
    static let TintColor = UIColor.colorFromHex(hex: "#5F9EA0")
    
    static func getBoldFont(size: CGFloat = Constants.FontSize) -> UIFont {
        guard let font = UIFont(name: Constants.RalewayFont, size: size) else {
            return UIFont.systemFont(ofSize: size)
        }
        return font.bold
    }
    
    static func getRegularFont(size: CGFloat = Constants.FontSize) -> UIFont {
        guard let font = UIFont(name: Constants.RalewayFont, size: size) else {
            return UIFont.systemFont(ofSize: size)
        }
        return font.regular
    }
}

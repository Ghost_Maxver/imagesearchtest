//
//  SearchTableViewCell.swift
//  ImageSearchTest
//
//  Created by DeveloperMBPRO on 9/19/19.
//  Copyright © 2019 Test project. All rights reserved.
//

import UIKit

protocol SearchTableViewCellDelegate: class {
    func searchTableViewCellDidPressRemove(button: UIButton, title: String, item: FlickrItem)
}

class SearchTableViewCell: UITableViewCell {
    private enum Constants {
        static let leadingImageOffset: CGFloat = 20
        static let widthImageView: CGFloat = 120
        static let heightImageView: CGFloat = 90
        
        static let trailingButtonOffset: CGFloat = 20
        static let widthButton: CGFloat = 40
        static let heightButton: CGFloat = 40
        static let imageButton = UIImage(named: "RemoveCell")
        
        static let defaultTitleText = "N/A"
        static let leadingTitleOffset: CGFloat = 10
        static let topAndBottomTitleOffset: CGFloat = 8
    }
    
    private var contentImage: UIImageView!
    private var title: UILabel!
    private var removeButton: UIButton!
    private var item: FlickrItem?
    
    public var delegate: SearchTableViewCellDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCell(item: FlickrItem) {
        self.item = item
        setupValue()
    }

    override func prepareForReuse() {
        contentImage.image = nil
        title.text = Constants.defaultTitleText
    }
    
    //MARK: Private
    
    private func setupValue() {
        guard let item = item else { return }
        title.text = item.title
        _ = BusinessLayer.shared.webManager.loadImage(url: item.getImageUrl) { (imageData, error) in
            if let imageData = imageData {
                DispatchQueue.main.async {
                    self.contentImage.image = UIImage(data: imageData)
                }
            }
            if let error = error {
                print(error.localizedDescription)
            }
        }
    }
}

// MARK: Actions
extension SearchTableViewCell {
    @objc private func didPressRemoveButton(_ button: UIButton) {
        if let item = self.item, let title = self.title.text {
            delegate?.searchTableViewCellDidPressRemove(button: button, title: title, item: item)
        }
    }
}

// MARK: Setup UI
extension SearchTableViewCell {
    private func setupView() {
        contentImage = UIImageView()
        addSubview(contentImage)
        contentImage.translatesAutoresizingMaskIntoConstraints = false
        contentImage.leftAnchor.constraint(equalTo: leftAnchor,
                                           constant: Constants.leadingImageOffset).isActive = true
        contentImage.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        contentImage.widthAnchor.constraint(equalToConstant: Constants.widthImageView).isActive = true
        contentImage.heightAnchor.constraint(equalToConstant: Constants.heightImageView).isActive = true
        
        removeButton = UIButton()
        removeButton.addTarget(self, action: #selector(didPressRemoveButton), for: .touchUpInside)
        addSubview(removeButton)
        removeButton.setImage(Constants.imageButton, for: .normal)
        removeButton.translatesAutoresizingMaskIntoConstraints = false
        removeButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        removeButton.rightAnchor.constraint(equalTo: rightAnchor,
                                            constant: -Constants.trailingButtonOffset).isActive = true
        removeButton.widthAnchor.constraint(equalToConstant: Constants.widthButton).isActive = true
        removeButton.heightAnchor.constraint(equalToConstant: Constants.heightButton).isActive = true
        
        title = UILabel()
        title.numberOfLines = 0
        title.text = Constants.defaultTitleText
        title.font = AppConstants.getRegularFont()
        addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        title.leftAnchor.constraint(equalTo: contentImage.rightAnchor,
                                    constant: Constants.leadingTitleOffset).isActive = true
        title.rightAnchor.constraint(equalTo: removeButton.leftAnchor).isActive = true
        title.topAnchor.constraint(equalTo: topAnchor,
                                   constant: Constants.topAndBottomTitleOffset).isActive = true
        title.bottomAnchor.constraint(equalTo: bottomAnchor,
                                      constant: -Constants.topAndBottomTitleOffset).isActive = true
    }
}

//
//  SearchViewController.swift
//  ImageSearchTest
//
//  Created by DeveloperMBPRO on 9/19/19.
//  Copyright © 2019 Test project. All rights reserved.
//

import UIKit
import RealmSwift

class SearchViewController: BaseViewController {
    private enum Constants {
        static let titleController = "Welcome"
        
        static let searchPlaceholder = "Search Image"
        static let searchTopOffset: CGFloat = 20
        static let searchLeadingAndTrailingOffset: CGFloat = 40
        static let searchImage = UIImage(named: "Search")
        static let searchImageWidth: CGFloat = 25
        static let searchLeadingImageOffset: CGFloat = 8
        static let searchImageTopAndBottomOffset: CGFloat = 5
        static let searchCornerRadius: CGFloat = 10
        
        static let tableViewTopOffset: CGFloat = 20
        static let cellHeight: CGFloat = 100
    }
    
    private enum AlertConstants {
        static let no = "No"
        static let yes = "Yes"
        static let deleteMessage = "Are you sure you want to delete the search result "
        static let emptyTextSearch = "To search, you must enter a query"
    }
    
    private var tableView: UITableView!
    private var searchField: UITextField!
    private var items: Results<FlickrItem>
    private var notificationToken: NotificationToken?

    init() {
        self.items = BusinessLayer.shared.realm.getFlickrItems()
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        connectNotificationUpdate()
    }
    
    override func configureView() {
        super.configureView()
        setupNavigation()
        setupSearchTextField()
        setupTableView()
    }
    
    private func startSearch(text: String) {
        if text.isEmpty {
            UIAlertController.showAlert(message: AlertConstants.emptyTextSearch)
            return
        }
        tableView.setContentOffset(.zero, animated: true)
        openProgressHUD(view: view)
        _ = BusinessLayer.shared.webManager.searchImage(text: text, completion: { [weak self] (_, error) in
            DispatchQueue.main.async {
                self?.dismissProgressHUD()
                if let error = error {
                    self?.proceedError(error: error)
                }
            }
        })
    }
    
    private func connectNotificationUpdate() {
        notificationToken = items.observe { [weak self] (changes) in
            guard let tableView = self?.tableView else { return }
            DispatchQueue.main.async {
                switch changes {
                case .initial:
                    tableView.reloadData()
                case .update(_, let deletions, let insertions, let modifications):
                    tableView.beginUpdates()
                    tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }), with: .automatic)
                    tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}), with: .automatic)
                    tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }), with: .automatic)
                    tableView.endUpdates()
                case .error(let error as NSError):
                    self?.proceedError(error: error)
                }
            }
        }
    }
}

extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        if let text = textField.text {
            startSearch(text: text)
        }
        textField.text = ""
        return true
    }
}

extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(withType: SearchTableViewCell.self, for: indexPath)
        cell.setupCell(item: items[indexPath.row])
        cell.selectionStyle = .none
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Recently"
        } else {
            return ""
        }
    }
}

extension SearchViewController: SearchTableViewCellDelegate {
    func searchTableViewCellDidPressRemove(button: UIButton, title: String, item: FlickrItem) {
        UIAlertController.showAlert(title: "Warning",
                                    message: AlertConstants.deleteMessage + "\"\(title)\"",
                                    cancelButtonTitle: AlertConstants.no,
                                    withTitleButtons: [AlertConstants.yes]) { (buttonIndex) in
                                        if buttonIndex == 1 {
                                            BusinessLayer.shared.realm.deleteFlickItem(item: item)
                                        }
        }
    }
}

// MARK: Setup UI
extension SearchViewController {
    private func setupNavigation() {
        title = Constants.titleController
        navigationController?.navigationBar.titleTextAttributes = [.font: AppConstants.getBoldFont()]
    }
    
    private func setupSearchTextField() {
        let searchContainer = UIView()
        searchContainer.backgroundColor = .white
        searchContainer.layer.cornerRadius = Constants.searchCornerRadius
        searchContainer.clipsToBounds = true
        view.addSubview(searchContainer)
        searchContainer.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 11.0, *) {
            searchContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,
                                                 constant: Constants.searchTopOffset).isActive = true
        } else {
            searchContainer.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor,
                                                 constant: Constants.searchTopOffset).isActive = true
        }
        searchContainer.leftAnchor.constraint(equalTo: view.leftAnchor,
                                              constant: Constants.searchLeadingAndTrailingOffset).isActive = true
        searchContainer.rightAnchor.constraint(equalTo: view.rightAnchor,
                                               constant: -Constants.searchLeadingAndTrailingOffset).isActive = true
        let searchImage = UIImageView(image: Constants.searchImage)
        searchImage.contentMode = .scaleAspectFit
        searchContainer.addSubview(searchImage)
        searchImage.translatesAutoresizingMaskIntoConstraints = false
        searchImage.leftAnchor.constraint(equalTo: searchContainer.leftAnchor,
                                          constant: Constants.searchLeadingImageOffset).isActive = true
        searchImage.topAnchor.constraint(equalTo: searchContainer.topAnchor, constant: Constants.searchImageTopAndBottomOffset).isActive = true
        searchImage.widthAnchor.constraint(equalToConstant: Constants.searchImageWidth).isActive = true
        searchImage.bottomAnchor.constraint(equalTo: searchContainer.bottomAnchor, constant: -Constants.searchImageTopAndBottomOffset).isActive = true
        
        searchField = UITextField()
        searchField.backgroundColor = .white
        searchField.borderStyle = .roundedRect
        searchField.placeholder = Constants.searchPlaceholder
        searchField.delegate = self
        searchField.returnKeyType = .search
        searchContainer.addSubview(searchField)
        searchField.translatesAutoresizingMaskIntoConstraints = false
        searchField.topAnchor.constraint(equalTo: searchContainer.topAnchor).isActive = true
        searchField.bottomAnchor.constraint(equalTo: searchContainer.bottomAnchor).isActive = true
        searchField.leftAnchor.constraint(equalTo: searchImage.rightAnchor).isActive = true
        searchField.rightAnchor.constraint(equalTo: searchContainer.rightAnchor).isActive = true
    }
    
    private func setupTableView() {
        tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.rowHeight = Constants.cellHeight
        
        tableView.register(SearchTableViewCell.self,
                           forCellReuseIdentifier: SearchTableViewCell.reuseIdentifier)
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: searchField.bottomAnchor,
                                       constant: Constants.tableViewTopOffset).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        if #available(iOS 11.0, *) {
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            tableView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor).isActive = true
        }
    }
}

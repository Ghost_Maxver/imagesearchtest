//
//  BaseViewController.swift
//  ImageSearchTest
//
//  Created by DeveloperMBPRO on 9/19/19.
//  Copyright © 2019 Test project. All rights reserved.
//

import UIKit
import JGProgressHUD

class BaseViewController: UIViewController {
    
    private let hud = JGProgressHUD(style: .dark)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    func configureView() {
        view.backgroundColor = AppConstants.MainColor
    }
    
    func openProgressHUD(text: String = "Loading", view: UIView, blockTouches: Bool = true) {
        hud.hudView.backgroundColor = AppConstants.TintColor
        hud.textLabel.text = text
        if !blockTouches {
            hud.tapOutsideBlock = { (hud) in
                self.dismissProgressHUD()
            }
        }
        hud.show(in: view)
    }
    
    func dismissProgressHUD() {
        hud.dismiss(animated: true)
    }
    
    func proceedError(error: NSError) {
        UIAlertController.showAlert(message: error.localizedDescription)
    }

}

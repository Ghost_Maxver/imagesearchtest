//
//  FlickrItem.swift
//  ImageSearchTest
//
//  Created by DeveloperMBPRO on 9/19/19.
//  Copyright © 2019 Test project. All rights reserved.
//

import RealmSwift

class FlickrItem: Object {
    
    @objc dynamic var id: String = ""
    @objc dynamic var title: String = "N/A"
    @objc dynamic var farm: Int = 0
    @objc dynamic var server: String = ""
    @objc dynamic var secret: String = ""
    @objc dynamic var timestamp: Date = Date()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    var getImageUrl: URL {
        return URL(string:"https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)_c.jpg")!
    }
}

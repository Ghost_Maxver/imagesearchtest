//
//  NSError+Extensions.swift
//  ImageSearchTest
//
//  Created by DeveloperMBPRO on 9/21/19.
//  Copyright © 2019 Test project. All rights reserved.
//

import Foundation

extension NSError {
    
    enum Code: Int {
        case Unknown = 0
        case NoData
        case ParseJSON
        case ParseValue
        case ApiKey
        case NoFoundPhoto
    }
    
    private static let Domain = Bundle.main.bundleIdentifier ?? "error.domain"
    
    static func errorWithCode(errorCode: Code, message: String?) -> NSError {
        return NSError(domain: NSError.Domain, code: errorCode.rawValue, userInfo: [NSLocalizedDescriptionKey: message ?? "Unknown error"])
    }
}

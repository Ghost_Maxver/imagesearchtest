//
//  UITableView+Extensions.swift
//  ImageSearchTest
//
//  Created by DeveloperMBPRO on 9/19/19.
//  Copyright © 2019 Test project. All rights reserved.
//

import UIKit

extension UITableView {
    func dequeueCell<T: CellIdentifying>(withType type: T.Type, for path: IndexPath) -> T {
        let cell = self.dequeueReusableCell(withIdentifier: type.reuseIdentifier, for: path)
        return cell as! T
    }
}

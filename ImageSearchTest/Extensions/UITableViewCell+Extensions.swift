//
//  UITableViewCell+Extensions.swift
//  ImageSearchTest
//
//  Created by DeveloperMBPRO on 9/19/19.
//  Copyright © 2019 Test project. All rights reserved.
//

import UIKit

extension UITableViewCell: CellIdentifying {
    
    static var nibName: String {
        let cellPathName = NSStringFromClass(self)
        let cellName = cellPathName.components(separatedBy: ".").last!
        return cellName
    }
    
    static var reuseIdentifier: String {
        return nibName
    }
    
    static var nibForCell: UINib {
        return UINib.init(nibName: reuseIdentifier, bundle: nil)
    }
    
}

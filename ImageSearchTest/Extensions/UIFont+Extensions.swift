//
//  UIFont+Extensions.swift
//  ImageSearchTest
//
//  Created by DeveloperMBPRO on 9/20/19.
//  Copyright © 2019 Test project. All rights reserved.
//

import UIKit

extension UIFont {
    var bold: UIFont {
        return withWeight(.bold)
    }
    var regular: UIFont {
        return withWeight(.regular)
    }
    
    private func withWeight(_ weight: UIFont.Weight) -> UIFont {
        var attributes = fontDescriptor.fontAttributes
        var traits = (attributes[.traits] as? [UIFontDescriptor.TraitKey: Any]) ?? [:]
        
        traits[.weight] = weight
        
        attributes[.name] = nil
        attributes[.traits] = traits
        attributes[.family] = familyName
        
        let descriptor = UIFontDescriptor(fontAttributes: attributes)
        
        return UIFont(descriptor: descriptor, size: pointSize)
    }
}

//
//  RealmHelper.swift
//  ImageSearchTest
//
//  Created by DeveloperMBPRO on 9/19/19.
//  Copyright © 2019 Test project. All rights reserved.
//

import Foundation
import RealmSwift

class RealmHelper: NSObject {
    private enum Constants {
        static let instanceAddress = "imagesearchtest.us1.cloud.realm.io"
    }
    
    private var realm: Realm = try! Realm()
    
    func getFlickrItems() -> Results<FlickrItem> {
        let items = self.realm.objects(FlickrItem.self).sorted(byKeyPath: "timestamp", ascending: false)
        #if DEBUG
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            _ = self.addNewFlickrItem(id: "4203199462", title: "Test Search1", farm: 3, server: "2800", secret: "c1ba188403")
        }
        #endif
        return items
    }
    
    func addNewFlickrItem(id: String, title: String, farm: Int, server: String, secret: String) -> FlickrItem? {
        var item: FlickrItem?
        DispatchQueue.main.async {
            do {
                try self.realm.write {
                    item = FlickrItem()
                    item?.id = id
                    item?.title = title
                    item?.farm = farm
                    item?.server = server
                    item?.secret = secret
                    self.realm.add(item!, update: .all)
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return item
    }
    
    func deleteFlickItem(item: FlickrItem) {
        DispatchQueue.main.async {
            do {
                try self.realm.write {
                    self.realm.delete(item)
                }
            } catch let error as NSError {
                print("Error \(error.localizedDescription)")
            }
        }
    }
}

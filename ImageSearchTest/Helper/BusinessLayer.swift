//
//  BusinessLayer.swift
//  ImageSearchTest
//
//  Created by DeveloperMBPRO on 9/21/19.
//  Copyright © 2019 Test project. All rights reserved.
//

import Foundation

class BusinessLayer: NSObject {
    static let shared = BusinessLayer()
    
    let webManager = WebManager()
    let realm = RealmHelper()
    
    private override init() {}
}

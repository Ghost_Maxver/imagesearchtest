//
//  WebManager.swift
//  ImageSearchTest
//
//  Created by DeveloperMBPRO on 9/19/19.
//  Copyright © 2019 Test project. All rights reserved.
//

import Foundation

class WebManager: NSObject {
    private enum FlickrAPIValues {
        static let SearchMethod = "flickr.photos.search"
        static let APIKey = "49a4eeac3ef4745cc65c94ec7f367a1a"
        static let ResponseFormat = "json"
        static let DisableJSONCallback = "1"
        static let MediumURL = "url_m"
        static let SafeSearch = "1"
        static let Sort = "relevance"
    }
    
    private enum FlickrAPIKeys {
        static let SearchMethod = "method"
        static let APIKey = "api_key"
        static let ResponseFormat = "format"
        static let DisableJSONCallback = "nojsoncallback"
        static let SafeSearch = "safe_search"
        static let Text = "text"
        static let Sort = "sort"
    }
    
    private let baseUrl = URL(string: "https://api.flickr.com/services/rest/")!
    
    func searchImage(text: String, completion: @escaping (_ item: FlickrItem?, _ error: NSError?) -> Void) -> URLSessionDataTask? {
        let url = getUrlForSearchImage(text: text)
        let task = URLSession.shared.dataTask(with: url) {  (data, response, error) in
            self.processResponse(data: data, response: response, error: error) { (response, error) in
                var item: FlickrItem? = nil
                var errorCompletion: NSError? = nil
                if let response = response,
                    let stat = response["stat"] as? String,
                    stat == "ok" {
                    if let header = response["photos"] as? [String: Any], let photos = header["photo"] as? [[String: Any]], photos.count > 0 {
                        let photo = photos[Int.random(in: 0..<photos.count)]
                        if let id = photo["id"] as? String,
                            let title = photo["title"] as? String,
                            let farm = photo["farm"] as? Int,
                            let server = photo["server"] as? String,
                            let secret = photo["secret"] as? String {
                            item = BusinessLayer.shared.realm.addNewFlickrItem(id: id, title: title, farm: farm, server: server, secret: secret)
                        } else {
                            errorCompletion = NSError.errorWithCode(errorCode: .ParseValue, message: "Could not parse value photo")
                        }
                    } else {
                        errorCompletion = NSError.errorWithCode(errorCode: .NoFoundPhoto, message: "No photos found for \(text)")
                    }
                } else {
                    errorCompletion = NSError.errorWithCode(errorCode: .ApiKey, message: "Wrong api-key.")
                }
                if let error = error {
                    errorCompletion = error
                }
                completion(item, errorCompletion)
            }
        }
        task.resume()
        return task
    }
    
    func loadImage(url: URL, completion: @escaping (_ imageData: Data?, _ error: NSError?) -> Void) -> URLSessionDataTask? {
        let session = URLSession(configuration: getUrlSessionConfigForDownload())
        let task = session.dataTask(with: url) { (data, response, error) in
            var dataCompletion: Data? = nil
            var errorCompletion: NSError? = nil
            if let data = data {
                dataCompletion = data
            }
            if let error = error as NSError? {
                errorCompletion = error
            }
            completion(dataCompletion, errorCompletion)
        }
        task.resume()
        return task
    }
    
    private func processResponse(data: Data?, response: URLResponse?, error: Error?, completion: (_ dictionary: [String: Any]?, _ error: NSError?) -> Void) {
        var dictionary: [String: Any]? = nil
        var errorCompletion: NSError? = nil
        if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode >= 200 || httpResponse.statusCode <= 300 {
            if let data = data {
                do {
                    dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
                } catch {
                    errorCompletion = NSError.errorWithCode(errorCode: .ParseJSON, message: "Could not parse the data as JSON")
                }
            } else {
                errorCompletion = NSError.errorWithCode(errorCode: .NoData, message: "No data was returned by the request!")
            }
        }
        if let error = error as NSError? {
           errorCompletion = error
        }
        completion(dictionary, errorCompletion)
    }
    
    private func getUrlSessionConfigForDownload() -> URLSessionConfiguration {
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = .returnCacheDataElseLoad
        return config
    }

    private func getUrlForSearchImage(text: String) -> URL {
        var urlComponents = URLComponents(url: baseUrl, resolvingAgainstBaseURL: true)
        urlComponents?.queryItems = [
            URLQueryItem(name: FlickrAPIKeys.SearchMethod, value: FlickrAPIValues.SearchMethod),
            URLQueryItem(name: FlickrAPIKeys.APIKey, value: FlickrAPIValues.APIKey),
            URLQueryItem(name: FlickrAPIKeys.Text, value: text),
            URLQueryItem(name: FlickrAPIKeys.ResponseFormat, value: FlickrAPIValues.ResponseFormat),
            URLQueryItem(name: FlickrAPIKeys.DisableJSONCallback, value: FlickrAPIValues.DisableJSONCallback),
            URLQueryItem(name: FlickrAPIKeys.SafeSearch, value: FlickrAPIValues.SafeSearch),
            URLQueryItem(name: FlickrAPIKeys.Sort, value: FlickrAPIValues.Sort)
        ]
        return urlComponents?.url ?? baseUrl
    }
}
